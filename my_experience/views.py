from django.shortcuts import render


# Enter your name here
mhs_name = 'Tegar Muhammad Soekarno' # TODO Implement this


# Create your views here.
def index(request):
    response = {'name': mhs_name}
    return render(request, 'my_experience.html', response)

def back():
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


